import React from 'react'
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Toolbar from '@mui/material/Toolbar';
import {createTheme, Grid, ThemeProvider} from "@mui/material";
import { indigo } from '@mui/material/colors';

const theme = createTheme({
        palette: {
            info: {
                main: indigo[900],
            },
        },
    },
);

export class MyNavBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            userName: "LOGO",
        }

        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    async logoutUser(token) {
        const response = await fetch(this.props.api_url + 'auth/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        });
        return await response.json();
    }

    async handleLogoutClick() {
        await this.logoutUser(this.props.getToken());
        this.props.setToken(null);
        sessionStorage.clear();
        this.props.navigate('/dashboard', {replace: true});
    }

    async currentUser() {
        let currentUser = sessionStorage.getItem('user');
        if ( currentUser == null) {
            //this.me(this.props.getToken()).then((result) => {sessionStorage.setItem('user', JSON.stringify(result))});
            currentUser = await this.me(this.props.getToken());
            sessionStorage.setItem('user', JSON.stringify(currentUser));
            return currentUser;
        }
        return JSON.parse(currentUser);
    }

    componentDidMount() {
        this.currentUser().then((result) => {this.setState({userName: result.name})});
    }

    async me(token) {
        const response = await fetch(this.props.api_url + 'me', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        });
        return await response.json();
    }

    render() {
        const location = window.location.pathname;
        return (
                <AppBar position="static">
                    <Toolbar>
                        <ThemeProvider theme={theme}>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                                <Typography variant="h6" color={indigo[900]}>
                                    {this.state.userName}
                                </Typography>
                            </Grid>
                            <Grid item xs={1}>
                                <Button color={location === '/dashboard' ? "inherit" : "info"} href="/dashboard" >
                                    Dashboard
                                </Button>
                            </Grid>
                            <Grid item xs={1}>
                                <Button color={location === '/exercises' ? "inherit" : "info"} href="/exercises" >
                                    Exercises
                                </Button>
                            </Grid>
                            <Grid item xs={1}>
                                <Button color={location === '/workouts' ? "inherit" : "info"} href="/workouts" >
                                    Workouts
                                </Button>
                            </Grid>
                            <Grid item xs={7}></Grid>
                            <Grid item xs={1}>
                                <Button color="info" onClick={this.handleLogoutClick}>Logout</Button>
                            </Grid>
                        </Grid>
                        </ThemeProvider>
                    </Toolbar>
                </AppBar>
        );
    }
}