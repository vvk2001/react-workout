import React from 'react'
import PropTypes from "prop-types";
import {
    Alert, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Grid, IconButton, MenuItem,
    Select,
    styled,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead, TablePagination,
    TableRow, TableSortLabel
} from "@mui/material";
import Typography from "@mui/material/Typography";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';

const Actions = { ADD: 'Add', EDIT: 'Edit'};

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

export class Workouts extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            action: Actions.ADD,
            confirmDeleteDialog: false,
            dialogExID: {},
            dialogInputs: {count1: 0, count2: 0, weight1: 0, weight2: 0},
            dialogOpen: false,
            dialogCount2show: false,
            dialogWeight1show: false,
            dialogWeight2show: false,
            currentPage: 0,
            error:null,
            exercises: [],
            sortOrder: 'desc',
            success: null,
            rowCurrent: null,
            rows: [],
            rowCount: 10,
            rowsPerPage: 5,
            rowToDelete: null,
            usedExercises: [],
            usedExercise: 0,
        };

        this.checkDialogInputsVisible = this.checkDialogInputsVisible.bind(this);

        this.closeError = this.closeError.bind(this);
        this.closeSuccess = this.closeSuccess.bind(this);

        this.fetchExercises = this.fetchExercises.bind(this);
        this.fetchWorkouts = this.fetchWorkouts.bind(this);

        this.handleActionDialog = this.handleActionDialog.bind(this);
        this.handleChangeDialogInputs = this.handleChangeDialogInputs.bind(this);
        this.handleChangeDialogExId = this.handleChangeDialogExId.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);

        this.handleClickConfirmDeleteClose = this.handleClickConfirmDeleteClose.bind(this);
        this.handleClickConfirmDeleteOpen = this.handleClickConfirmDeleteOpen.bind(this);
        this.handleClickConfirmDeleteYes = this.handleClickConfirmDeleteYes.bind(this);
        this.handleClickDialogClose = this.handleClickDialogClose.bind(this);
        this.handleClickDialogOpen = this.handleClickDialogOpen.bind(this);
        this.handleClickEditDialogOpen = this.handleClickEditDialogOpen.bind(this);

        this.handleUsedExClick = this.handleUsedExClick.bind(this);
        this.setSortOrder = this.setSortOrder.bind(this);
    }

    componentDidMount() {
        this.fetchWorkouts();
        this.fetchExercises();
    }

    //Проверяет какие поля ввода показывать в зависимости от типа упражнения
    checkDialogInputsVisible(ex_type) {
        switch (ex_type) {
            case 0:
                this.setState({dialogCount2show: false, dialogWeight1show: false, dialogWeight2show: false});
                break;
            case 1:
                this.setState({dialogCount2show: true, dialogWeight1show: false, dialogWeight2show: false});
                break;
            case 2:
                this.setState({dialogCount2show: false, dialogWeight1show: true, dialogWeight2show: false});
                break;
            case 3:
                this.setState({dialogCount2show: true, dialogWeight1show: true, dialogWeight2show: true});
                break;
            default:
                break;
        }
    }

    closeError() {
        this.setState({error: null});
    }

    closeSuccess() {
        this.setState({success: null});
    }

    fetchExercises() {
        fetch(this.props.api_url + 'exercises/index', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + this.props.getToken(),
                'Content-Type': 'application/json',
            },
        }).then(res => res.json())
            .then((result) => {
                    let exercises = [];
                    result.forEach(e => {
                        exercises.push(e);
                    });
                    if(exercises.length === 0) exercises = [{ex_id: 0, ex_descr: "None", ex_type: 0}];
                    this.setState({exercises: exercises, dialogExID: exercises[0]});
                },
                () => {
                    this.setState({error: 'Can not fetch exercises'});
                })
    }

    fetchWorkouts(sortOrder = 'desc', fltExercise = 0, limit = 5, offset = 0) {
        fetch(this.props.api_url + 'workouts/index', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + this.props.getToken(),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({sortOrder: sortOrder, fltExercise: fltExercise, limit: limit, offset: offset})
        }).then(res => res.json())
            .then(({workouts, usedExercises, fltExercise, count}) => {
                    let rows = [];
                    let tmp_usedExercises = [];
                    workouts.forEach(e => {
                        rows.push(e);
                    });
                    usedExercises.forEach(e => {
                        tmp_usedExercises.push(e);
                    })
                    this.setState({rows: rows, usedExercises: tmp_usedExercises, usedExercise: fltExercise, rowCount: count, error: null, editDialogOpen: false, dialogOpen: false, confirmDeleteDialog: false});
                },
                () => {
                    this.setState({error: 'Can not fetch workouts'});
                })
    }

    // Обработичик нажатия на кнопку Add / Edit диалогового окна добавления или удаления тренировки
    handleActionDialog() {
        if(this.state.action === Actions.EDIT) {
            fetch(this.props.api_url + 'workouts/update', {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + this.props.getToken(),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({w_id: this.state.rowCurrent.w_id, ex_id: this.state.dialogExID.ex_id, count1: this.state.dialogInputs.count1,
                    count2: this.state.dialogInputs.count2, weight1: this.state.dialogInputs.weight1, weight2: this.state.dialogInputs.weight2}),
            }).then(res => res.json())
                .then(() => {
                        this.setState({success: "Workout updated!"});
                        this.fetchWorkouts(this.state.sortOrder, this.state.usedExercise, this.state.rowsPerPage, this.state.currentPage);
                    },
                    () => {
                        this.setState({error: 'Can not edit Workout'});
                    })
        } else {
            fetch(this.props.api_url + 'workouts/store', {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + this.props.getToken(),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ex_id: this.state.dialogExID.ex_id, count1: this.state.dialogInputs.count1,
                    count2: this.state.dialogInputs.count2, weight1: this.state.dialogInputs.weight1, weight2: this.state.dialogInputs.weight2}),
            }).then(res => res.json())
                .then(() => {
                        this.setState({success: "Workout added!"});
                        this.fetchWorkouts(this.state.sortOrder, this.state.usedExercise, this.state.rowsPerPage, this.state.currentPage);
                    },
                    () => {
                        this.setState({error: 'Can not add Workout'});
                    })
        }
        this.setState({dialogOpen: false});
    }

    handleChangeRowsPerPage(event) {
        this.setState({rowsPerPage: parseInt(event.target.value, 10), currentPage: 0});
        this.fetchWorkouts(this.state.sortOrder, this.state.usedExercise, parseInt(event.target.value, 10), 0);
    }

    handleChangePage(event, page) {
        this.setState({currentPage: page});
        this.fetchWorkouts(this.state.sortOrder, this.state.usedExercise, this.state.rowsPerPage, page);
    }

    handleUsedExClick(event) {
        this.fetchWorkouts(this.state.sortOrder, event.target.value, this.state.rowsPerPage, this.state.currentPage);
        this.setState({usedExercise: event.target.value});
    }

    handleChangeDialogInputs(event) {
        let dialogInputs = this.state.dialogInputs;
        if((event.target.id === 'count1') || (event.target.id === 'count2')) {
            dialogInputs[event.target.id] = parseInt(event.target.value);
        } else {
            dialogInputs[event.target.id] = parseFloat(event.target.value);
        }
        if(dialogInputs[event.target.id]!==dialogInputs[event.target.id]) dialogInputs[event.target.id] = 0;
        this.setState({dialogInputs: dialogInputs});
    }

    handleChangeDialogExId(event) {
        this.checkDialogInputsVisible(event.target.value.ex_type);
        this.setState({dialogExID: event.target.value});
    }

    handleClickConfirmDeleteClose() {
        this.setState({confirmDeleteDialog: false})
    }

    handleClickConfirmDeleteOpen(event, row) {
        this.setState({confirmDeleteDialog: true, rowToDelete: row});
    }

    handleClickConfirmDeleteYes() {
        this.setState({confirmDeleteDialog: false});
        fetch(this.props.api_url + 'workouts/destroy/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({w_id: this.state.rowToDelete.w_id})
        }).then(res => res.json())
            .then(() => {
                    this.setState({success: "Workout deleted!"});
                    this.fetchWorkouts(this.state.sortOrder, this.state.usedExercise, this.state.rowsPerPage, this.state.currentPage);
                },
                () => {
                    this.setState({error: 'Can not delete Workout'});
                });
    }

    handleClickDialogOpen() {
        this.checkDialogInputsVisible(this.state.exercises[0].ex_type);
        this.setState({action: Actions.ADD, dialogOpen: true, dialogInputs: {count1: 0, count2: 0, weight1: 0, weight2: 0}, dialogExID: this.state.exercises[0]});
    }

    handleClickEditDialogOpen(event, row) {
        let exID = null;
        this.state.exercises.some(e => {
            if (e.ex_id === row.ex_id) {
                exID = e;
                return true;
            }
            return false;
        })
        this.checkDialogInputsVisible(exID.ex_type);
        this.setState({action: Actions.EDIT, dialogOpen: true, dialogExID: exID, rowCurrent: row, dialogInputs: {count1: row.count1, count2: row.count2, weight1: row.weight1, weight2: row.weight2},});
    }

    handleClickDialogClose() {
        this.setState({dialogOpen: false});
    }

    setSortOrder() {
        let sortOrder = this.state.sortOrder === 'asc' ? 'desc' : 'asc';
        this.setState({sortOrder: sortOrder});
        this.fetchWorkouts(sortOrder, this.state.usedExercise, this.state.rowsPerPage, this.state.currentPage);
    }

    render() {
        return (
            <Grid container spacing={2}>
                {this.state.error && <Grid item xs={12}> <Alert onClose={() => this.closeError()} severity="error" variant="outlined">{this.state.error}</Alert></Grid>}
                {this.state.success && <Grid item xs={12}> <Alert onClose={() => this.closeSuccess()} severity="success" variant="outlined">{this.state.success}</Alert></Grid>}
                <Grid item xs={10}></Grid>
                <Grid item xs={2}>
                    <Button variant="contained" onClick={this.handleClickDialogOpen}>
                        Add Workout
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>

                                    <TableCell align="left" style={{borderBottom: "solid"}} width="15%">
                                        <Typography variant="h6" fontWeight={"bold"}>
                                            <TableSortLabel active={true} direction={this.state.sortOrder} onClick={this.setSortOrder}></TableSortLabel>
                                            Date
                                        </Typography>
                                    </TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="30%">
                                        <Typography variant="h6" fontWeight={"bold"}>
                                            Exercise
                                            <Select style={{width: "50%", marginLeft: 4}} id="used_ex_id" value={this.state.usedExercise} onChange={this.handleUsedExClick}>
                                                <MenuItem key={"mi0"} value={0}>All</MenuItem>
                                                {this.state.usedExercises.map((e, i) => <MenuItem key={"mi"+i} value={e.ex_id}>{e.ex_descr}</MenuItem> )}
                                            </Select>
                                        </Typography>
                                    </TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="10%"><Typography variant="h6" fontWeight={"bold"}>Count One</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="10%"><Typography variant="h6" fontWeight={"bold"}>Weigh One</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="10%"><Typography variant="h6" fontWeight={"bold"}>Count Two</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="10%"><Typography variant="h6" fontWeight={"bold"}>Weigh Two</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="15%"><Typography variant="h6" fontWeight={"bold"}>Actions</Typography></TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    this.state.rows.map((row) => (
                                        <StyledTableRow key={row.w_id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                            <TableCell component="th" scope="row">
                                                {row.created_at}
                                            </TableCell>
                                            <TableCell align="left">{row.ex_descr}</TableCell>
                                            <TableCell align="left">{row.count1}</TableCell>
                                            <TableCell align="left">{((row.ex_type === 2)||(row.ex_type === 3)) ? row.weight1 : <MoreHorizIcon/>}</TableCell>
                                            <TableCell align="left">{((row.ex_type === 1)||(row.ex_type === 3)) ? row.count2 : <MoreHorizIcon/>}</TableCell>
                                            <TableCell align="left">{(row.ex_type === 3) ? row.weight2 : <MoreHorizIcon/>}</TableCell>
                                            <TableCell>
                                                <IconButton color="primary" sx={{m: 0.5}} onClick={(event) => this.handleClickEditDialogOpen(event, row)}><ModeEditIcon/></IconButton>
                                                <IconButton color="error" sx={{m: 0.5}} onClick={(event) => this.handleClickConfirmDeleteOpen(event, row)}><DeleteForeverIcon /></IconButton>
                                            </TableCell>
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={12} display="flex" justifyContent="center">
                    <TablePagination
                        count={this.state.rowCount}
                        component="div"
                        page={this.state.currentPage}
                        rowsPerPage={this.state.rowsPerPage}
                        rowsPerPageOptions={[3, 5, 10, 25]}
                        onPageChange={this.handleChangePage}
                        onRowsPerPageChange={this.handleChangeRowsPerPage}
                    />
                </Grid>

                {/* Workout dialog */}
                <Dialog fullWidth open={this.state.dialogOpen} onClose={this.handleClickDialogClose}>
                    <DialogTitle>{this.state.action} Workout</DialogTitle>
                    <DialogContent>
                        <Select style={{minWidth: "90%"}} sx={{m: 2}} value={this.state.dialogExID} onChange={this.handleChangeDialogExId}>
                            {this.state.exercises.map((e, i) => <MenuItem key={"mi"+i} value={e}>{e.ex_descr}</MenuItem> )}
                        </Select>
                        <TextField style={{width: "90%"}} sx={{m: 2}}
                                   id="count1"
                            label="Count One"
                            value={this.state.dialogInputs.count1}
                            onChange={this.handleChangeDialogInputs}
                        />
                        {this.state.dialogWeight1show && <TextField style={{width: "90%"}} sx={{m: 2}}
                                   id="weight1"
                                   label="Weight One"
                                   value={this.state.dialogInputs.weight1}
                                   onChange={this.handleChangeDialogInputs}
                                   type="number"
                                   inputProps={{step: "0.01"}}
                        />}
                        {this.state.dialogCount2show &&<TextField style={{width: "90%"}} sx={{m: 2}}
                                   id="count2"
                                   label="Count Two"
                                   value={this.state.dialogInputs.count2}
                                   onChange={this.handleChangeDialogInputs}
                        />}
                        {this.state.dialogWeight2show && <TextField style={{width: "90%"}} sx={{m: 2}}
                                   id="weight2"
                                   label="Weight Two"
                                   value={this.state.dialogInputs.weight2}
                                   onChange={this.handleChangeDialogInputs}
                                   type="number"
                                   inputProps={{step: "0.01"}}
                        />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClickDialogClose}>Cancel</Button>
                        <Button onClick={this.handleActionDialog}>{this.state.action}</Button>
                    </DialogActions>
                </Dialog>

                {/* Confirm delete dialog*/}
                <Dialog open={this.state.confirmDeleteDialog} onClose={this.handleClickConfirmDeleteClose}>
                    <DialogTitle>Delete Exercise</DialogTitle>
                    <DialogContent>
                        {this.state.rowToDelete && <DialogContentText>Do you really want to delete Workout: <Box sx={{fontWeight: "bold", textTransform: 'uppercase'}}>{this.state.rowToDelete.ex_descr + ' ' + this.state.rowToDelete.created_at}</Box></DialogContentText>}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClickConfirmDeleteClose} autoFocus>No</Button>
                        <Button onClick={this.handleClickConfirmDeleteYes} color="warning">Yes</Button>
                    </DialogActions>
                </Dialog>

            </Grid>
        );
    }
}

Workouts.propTypes = {
    getToken: PropTypes.func.isRequired,
    api_url: PropTypes.string.isRequired,
}