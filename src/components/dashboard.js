import React from 'react'
import {Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend,} from 'chart.js'
import { Line } from 'react-chartjs-2';
import {FormHelperText, Grid, InputLabel, MenuItem, Select} from "@mui/material";
import PropTypes from "prop-types";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
)

export class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ex_type: 0,
            ex_id: 0,
            ex_id_children: [{ex_id: 0, ex_descr: "None"}],
            error: null,
            chartLabels: [],
            chartData: [],
        };

        this.handleExTypeChange = this.handleExTypeChange.bind(this);
        this.handleExIdChange = this.handleExIdChange.bind(this);
    }

    componentDidMount() {
        // Имитируем нажатие Select с типом упражнения
        this.handleExTypeChange({target: {value: 0}});
    }

    handleExTypeChange(event) {
        fetch(this.props.api_url + 'exercises/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({ex_type: event.target.value })
        }).then(res => res.json())
            .then((result) => {
                    let ex_id_children = [];

                    // Из всех данных нам нужны
                    // ex_id - id упражнения
                    // ex_descr - наименование упражнения
                    result.data.map( e => ex_id_children.push({ex_id: e.ex_id, ex_descr: e.ex_descr}));
                    if(ex_id_children.length === 0) ex_id_children = [{ex_id: 0, ex_descr: "None"}];
                    this.setState({ex_id_children: ex_id_children, ex_id: ex_id_children[0].ex_id});

                    // Имитируем нажатие Select с выбором самого верхнего упражнения
                    this.handleExIdChange({target: {value: ex_id_children[0].ex_id}});
                },
                (error) => {
                    this.setState({error: error});
                })
        this.setState({ex_type: event.target.value});
    }

    handleExIdChange(event) {
        if(event.target.value === 0) return;
        fetch(this.props.api_url + 'exercisesdata/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({ex_id: event.target.value })
        }).then(res => res.json())
            .then((result) => {
                    let cLabels = [];
                    let cData = [];
                    /**
                     * @param {{created_at:string, count1: int}} e
                     */
                result.data.forEach( e => {
                        cLabels.push(e.created_at);
                        cData.push(e.count1);
                    });
                    this.setState({chartLabels: cLabels, chartData: cData});
                },
                (error) => {
                    this.setState({error: error});
                })
        this.setState({ex_id: event.target.value});
    }

    render() {
        return (
            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <InputLabel id="ex_type-label">Type</InputLabel>
                    <Select style={{width: "100%"}} id="ex_type" value={this.state.ex_type} onChange={this.handleExTypeChange}>
                        <MenuItem value={0}>Without Weight</MenuItem>
                        <MenuItem value={1}>Separated Without Weight</MenuItem>
                        <MenuItem value={2}>With Weight</MenuItem>
                        <MenuItem value={3}>Separated With Weight</MenuItem>
                    </Select>
                    <FormHelperText>Type of exercises</FormHelperText>
                </Grid>
                <Grid item xs={2}>
                    <InputLabel id="ex_id-label">Exercise</InputLabel>
                    <Select style={{width: "100%"}} id="ex_id" value={this.state.ex_id} onChange={this.handleExIdChange}>
                        {this.state.ex_id_children.map((e, i) => <MenuItem key={"mi"+i} value={e.ex_id}>{e.ex_descr}</MenuItem> )}
                    </Select>
                    <FormHelperText>Exercise</FormHelperText>
                </Grid>
                <Grid item xs={12}>
                    <Line datasetIdKey='id' data={{
                        labels: this.state.chartLabels,
                        datasets: [{
                            id: 1,
                            label: "Count",
                            data: this.state.chartData,
                            backgroundColor: 'rgb(255, 99, 132)',
                            borderColor: 'rgb(255, 99, 132)',
                        }]
                    }}/>
                </Grid>
            </Grid>
        )
    }
}

Dashboard.propTypes = {
    getToken: PropTypes.func.isRequired,
    api_url: PropTypes.string.isRequired,
}