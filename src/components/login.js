import React from 'react'
import PropTypes from 'prop-types';
import {Alert} from "@mui/material";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import './login.css';

export class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: null,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        fetch(this.props.api_url + 'auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: this.state.email,  password: this.state.password })
        }).then(res => res.json())
            .then((result) => {
                    /**
                     * @param {{token:string}} e
                     */
                this.props.setToken(result.data.token);
                this.props.navigate('/dashboard', {replace: true});
            },
            (error) => {
                this.setState({error: error});
            })
    }

    render() {
        return (
            <div className="login-wrapper">
                <form onSubmit={this.handleSubmit}>
                    <h2 className="form-title">Login</h2>
                    {this.state.error && <Alert severity="error">Invalid Credentials</Alert>}
                    <TextField className="form-input"
                        required
                        id="outlined-email-input"
                        label="Email"
                               sx={{m: 1}}
                        onChange={event => this.setState( {email: event.target.value})}
                    />

                    <TextField className="form-input"
                        required
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                               sx={{m: 1}}
                        autoComplete="current-password"
                        onChange={event => this.setState( {password: event.target.value})}
                    />

                    <div>
                        <Button type="submit" variant="contained" sx={{m: 1}}>Login</Button>
                    </div>
                </form>
            </div>
        )
    }
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
    api_url: PropTypes.string.isRequired,
}