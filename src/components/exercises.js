import React from 'react'
import PropTypes from "prop-types";
import {
    Alert, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Grid, IconButton, MenuItem,
    Paper, Select,
    styled,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

function exTypeToString(type) {

    switch (type) {
        case 0:
            return "No weight";
        case 1:
            return "Separated without weight";
        case 2:
            return "With weight";
        case 3:
            return "Separated with weight";
        default:
            return "Undefined";
    }
}

export class Exercises extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addDialogOpen: false,
            addExType: 0,
            addExDescr: "",
            confirmDeleteDialog: false,
            editDialogOpen: false,
            editDialogExDescr: null,
            rowToDelete: null,
            rowToEdit:null,
            error: null,
            success: null,
            rows: [],
        };

        this.closeError = this.closeError.bind(this);
        this.closeSuccess = this.closeSuccess.bind(this);

        this.handleClickAddDialogOpen = this.handleClickAddDialogOpen.bind(this);
        this.handleClickAddDialogClose = this.handleClickAddDialogClose.bind(this);
        this.handleClickAddDialogAdd = this.handleClickAddDialogAdd.bind(this);
        this.handleChangeAddDialogDescr = this.handleChangeAddDialogDescr.bind(this);

        this.handleClickSelectType = this.handleClickSelectType.bind(this);

        this.handleClickConfirmDeleteClose = this.handleClickConfirmDeleteClose.bind(this);
        this.handleClickConfirmDeleteOpen = this.handleClickConfirmDeleteOpen.bind(this);
        this.handleClickConfirmDeleteYes = this.handleClickConfirmDeleteYes.bind(this);

        /*Edit Dialog*/
        this.handleClickEditDialogClose = this.handleClickEditDialogClose.bind(this);
        this.handleClickEditDialogOpen = this.handleClickEditDialogOpen.bind(this);
        this.handleClickEditDialogSave = this.handleClickEditDialogSave.bind(this);

        this.fetchExercises = this.fetchExercises.bind(this);
    };

    closeError() {
        this.setState({error: null});
    }

    closeSuccess() {
        this.setState({success: null});
    }

    componentDidMount() {
        this.fetchExercises();
    }

    fetchExercises() {
        fetch(this.props.api_url + 'exercises/index', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
        }).then(res => res.json())
            .then((result) => {
                    let rows = [];
                    result.forEach(e => {
                        rows.push({ex_id: e.ex_id, ex_descr: e.ex_descr, ex_type: e.ex_type, ex_type_txt: exTypeToString(e.ex_type)});
                    });
                    this.setState({rows: rows, error: null, editDialogOpen: false, addDialogOpen: false, confirmDeleteDialog: false});
                },
                (error) => {
                    this.setState({error: 'Can not fetch exercises'});
                })
    }

    handleClickAddDialogOpen() {
        this.setState({addDialogOpen: true});
    }

    handleClickAddDialogClose() {
        this.setState({addDialogOpen: false});
    }

    handleClickAddDialogAdd() {
        this.setState({addDialogOpen: false});
        fetch(this.props.api_url + 'exercises/store/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({ex_type: this.state.addExType, ex_descr: this.state.addExDescr})
        }).then(res => res.json())
            .then((result) => {
                    this.setState({success: "Exercise added!"});
                    this.fetchExercises();
                },
                (error) => {
                    this.setState({error: 'Can not add Exercise'});
                })
    }

    handleChangeAddDialogDescr(event) {
        this.setState({addExDescr: event.target.value});
    }

    handleClickSelectType(event) {
        this.setState({addExType: event.target.value});
    }

    handleClickConfirmDeleteYes() {
        this.setState({confirmDeleteDialog: false});
        fetch(this.props.api_url + 'exercises/destroy/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({ex_id: this.state.rowToDelete.ex_id})
        }).then(res => res.json())
            .then((result) => {
                    this.setState({success: "Exercise deleted!"});
                    this.fetchExercises();
                },
                (error) => {
                    this.setState({error: 'Can not delete exercise'});
                });
    }

    handleClickConfirmDeleteClose() {
        this.setState({confirmDeleteDialog: false})
    }

    handleClickConfirmDeleteOpen(event, row) {
        this.setState({confirmDeleteDialog: true, rowToDelete: row});
    }

    /*Edit Dialog*/
    handleClickEditDialogClose() {
        this.setState({editDialogOpen: false});
    }

    handleClickEditDialogSave() {
        this.setState({editDialogOpen: false});
        fetch(this.props.api_url + 'exercises/update/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.getToken(),
            },
            body: JSON.stringify({ex_id: this.state.rowToEdit, ex_type: this.state.addExType, ex_descr: this.state.addExDescr})
        }).then(res => res.json())
            .then(() => {
                    this.setState({success: "Exercise updated!"});
                    this.fetchExercises();
                },
                () => {
                    this.setState({error: 'Can not edit Exercise'});
                })
    }

    handleClickEditDialogOpen(event, row) {
        this.setState({editDialogOpen: true, rowToEdit: row.ex_id, addExType: row.ex_type, addExDescr: row.ex_descr});
    }

    render() {
        return (
            <Grid container spacing={2}>
                {this.state.error && <Grid item xs={12}> <Alert onClose={() => this.closeError()} severity="error" variant="outlined">{this.state.error}</Alert></Grid>}
                {this.state.success && <Grid item xs={12}> <Alert onClose={() => this.closeSuccess()} severity="success" variant="outlined">{this.state.success}</Alert></Grid>}
                <Grid item xs={10}></Grid>
                <Grid item xs={2}>
                    <Button variant="contained" onClick={this.handleClickAddDialogOpen}>
                        Add Exercise
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="65%"><Typography variant="h6" fontWeight={"bold"}>Exercises</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="23%"><Typography variant="h6" fontWeight={"bold"}>Type</Typography></TableCell>
                                    <TableCell align="left" style={{borderBottom: "solid"}} width="12%"><Typography variant="h6" fontWeight={"bold"}>Actions</Typography></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    this.state.rows.map((row) => (
                                        <StyledTableRow key={row.ex_id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                            <TableCell component="th" scope="row">
                                                {row.ex_descr}
                                            </TableCell>
                                            <TableCell align="left">{row.ex_type_txt}</TableCell>
                                            <TableCell>
                                                <IconButton color="primary" sx={{m: 0.5}} onClick={(event) => this.handleClickEditDialogOpen(event, row)}><ModeEditIcon/></IconButton>
                                                <IconButton color="error" sx={{m: 0.5}} onClick={(event) => this.handleClickConfirmDeleteOpen(event, row)}><DeleteForeverIcon /></IconButton>
                                            </TableCell>
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                {/* Add new exercise dialog */}
                <Dialog open={this.state.addDialogOpen} onClose={this.handleClickAddDialogClose}>
                    <DialogTitle>Add Exercise</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="ex_descr"
                            label="Exercise Description"
                            type="text"
                            fullWidth
                            variant="standard"
                            onChange={this.handleChangeAddDialogDescr}
                        />
                        <Select fullWidth id="ex_type" value={this.state.addExType} onChange={this.handleClickSelectType}>
                            <MenuItem value={0}>Without Weight</MenuItem>
                            <MenuItem value={1}>Separated Without Weight</MenuItem>
                            <MenuItem value={2}>With Weight</MenuItem>
                            <MenuItem value={3}>Separated With Weight</MenuItem>
                        </Select>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClickAddDialogClose}>Cancel</Button>
                        <Button onClick={this.handleClickAddDialogAdd}>Add</Button>
                    </DialogActions>
                </Dialog>
                {/* End new exercise dialog */}

                {/* Confirm delete dialog*/}
                <Dialog open={this.state.confirmDeleteDialog} onClose={this.handleClickConfirmDeleteClose}>
                    <DialogTitle>Delete Exercise</DialogTitle>
                    <DialogContent>
                        {this.state.rowToDelete && <DialogContentText>Do you really want to delete exercise: <Box sx={{fontWeight: "bold", textTransform: 'uppercase'}}>{this.state.rowToDelete.ex_descr}</Box></DialogContentText>}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClickConfirmDeleteClose} autoFocus>No</Button>
                        <Button onClick={this.handleClickConfirmDeleteYes} color="warning">Yes</Button>
                    </DialogActions>
                </Dialog>

                {/*Edit Dialog*/}
                <Dialog open={this.state.editDialogOpen} onClose={this.handleClickEditDialogClose}>
                    <DialogTitle>Edit Exercise</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="ex_descr"
                            label="Exercise Description"
                            type="text"
                            fullWidth
                            variant="standard"
                            onChange={this.handleChangeAddDialogDescr}
                            value={this.state.addExDescr}
                        />
                        <Select fullWidth id="ex_type" value={this.state.addExType} onChange={this.handleClickSelectType}>
                            <MenuItem value={0}>Without Weight</MenuItem>
                            <MenuItem value={1}>Separated Without Weight</MenuItem>
                            <MenuItem value={2}>With Weight</MenuItem>
                            <MenuItem value={3}>Separated With Weight</MenuItem>
                        </Select>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClickEditDialogClose}>Cancel</Button>
                        <Button onClick={this.handleClickEditDialogSave}>Save</Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        );
    }
}

Exercises.propTypes = {
    getToken: PropTypes.func.isRequired,
    api_url: PropTypes.string.isRequired,
}