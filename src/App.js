import React from 'react';
import { Route, Routes, Navigate, useNavigate } from 'react-router-dom';
import { Dashboard } from './components/dashboard';
import { Exercises } from './components/exercises';
import { Login } from './components/login';
import { MyNavBar } from './components/myNavBar'
import './App.css';
import {Workouts} from "./components/workouts";

function setToken(userToken) {
  sessionStorage.setItem('token', userToken);
}

function getToken() {
  return sessionStorage.getItem('token');
}

function App() {

    const api_url = 'http://localhost:8000/api/';
    const navigate = useNavigate();
    const token = getToken();

  if(!token) {
    return <Login setToken={setToken} navigate={navigate} api_url={api_url}/>
  }

  return (
      <>
        <MyNavBar setToken={setToken} getToken={getToken} navigate={navigate} api_url={api_url}/>
          <div className="wrapper">
              <Routes>
                  <Route path="/dashboard" element={<Dashboard getToken={getToken} api_url={api_url}/>} />
                  <Route path="/" element={<Navigate replace to="/dashboard" />} />
                  <Route path="/exercises" element={<Exercises getToken={getToken} api_url={api_url}/>} />
                  <Route path="/workouts" element={<Workouts getToken={getToken} api_url={api_url}/>} />
              </Routes>
          </div>
      </>
  );
}

export default App;
